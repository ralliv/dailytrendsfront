# README #

-. Proyecto DailyTrends .-

### Introducción ###

- DailyTrends is an app that shows news extracted fron de front pages of the main newspapers in Spain.
- This project is divided into two parts. In the current repository, there is the Frontend part of the app.
- v 1.0.0


### Setup ###

* Versions and dependencies
  - Nodejs: v 8.11.1 LTS
  - Angular 5: @angular/cli v 1.7.4
  - BootStrap: v 4.1.1

* Configurations
* Database configuration
* Tests
* Deployment instructions


### Contact ###

- villar.adrian.93@gmail.com